
public class Aufgabe4 {
	
	public static void main(String[] args) {
		int zahl = 5;
		System.out.print(romanNumerals(zahl));
	}
	
	//hier soll Ihr Quelltext hin
	private static char romanNumerals(int zahl) {
		char returnValue;
		
		switch (zahl) 
		{
			case 1:
				returnValue = 'I';
				break;
			case 5:
				returnValue = 'V';
				break;
			case 10:
				returnValue = 'X';
				break;
			case 50:
				returnValue = 'L';
				break;
			case 100:
				returnValue = 'C';
				break;
			case 500:
				returnValue = 'D';
				break;
			case 1000:
				returnValue = 'M';
				break;
			default:
				returnValue = '?';
				break;	
		}
		
		return returnValue;
	}
}
