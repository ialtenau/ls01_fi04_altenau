import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		//Variablendeklaration
		double a,h,v;
		
		//Eingabe
		Scanner scan = new Scanner(System.in);
		System.out.println("Volumenberechnung fuer eine gerade quadratische Pyramide");
		System.out.print("Bitte geben Sie die Kantenl�nge an: ");
		a = scan.nextDouble();
		System.out.print("Bitte geben Sie die Hoehe an: ");
		h = scan.nextDouble();
		
		//Verarbeitung
		v = volumen(a,h);
		
		//Ausgabe
		System.out.println("Das Volumen betraegt " + v);
		scan.close();
	}
	
	public static double volumen(double a, double h) {
		
		double v = 0;
		
		//hier soll ihr Quellcode hin
		v = (a*a*h)/3.0;
		
		//Ende ihr Quellcode
		
		return v;
	}

}
