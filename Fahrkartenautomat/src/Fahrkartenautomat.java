﻿import java.util.Scanner;

class Fahrkartenautomat {

	static Scanner tastatur = new Scanner(System.in);

	public static double fahrkartenbestellungErfassen() {
		double zuZahlenderBetrag = 0;
		int fahrkartenId;
		int anzahlKarten = 0;
		String[] fahrkartenBezeichnung = {
                "Einzelfahrschein Berlin AB",
                "Einzelfahrschein Berlin BC",
                "Einzelfahrschein Berlin ABC",
                "Kurzstrecke",
                "Tageskarte Berlin AB",
                "Tageskarte Berlin BC",
                "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC",
                "Kleingruppen-Tageskarte Berlin ABC"
        };
        double[] fahrkartenPreis = {
                2.90,
                3.30,
                3.60,
                1.90,
                8.60,
                9.00,
                9.60,
                23.50,
                24.30,
                24.90
        };

		
        for (int i = 0; i < fahrkartenBezeichnung.length; i++) {
        	System.out.println("Nummer " + (i+1) + ": " + fahrkartenBezeichnung[i] + ": " + fahrkartenPreis[i] + " Euro");
        }

        System.out.println("");
		System.out.print("Fahrkartennummer: ");
		fahrkartenId = tastatur.nextInt();
		
		while(fahrkartenId < 1 | fahrkartenId > fahrkartenBezeichnung.length) {
			System.out.println("Wert ungueltig. Nur Nummern von 1 - " + fahrkartenBezeichnung.length + " erlaubt.");
			fahrkartenId = tastatur.nextInt();
		}
			
		fahrkartenId -= 1;
		System.out.println("Ihre Auswahl: " + fahrkartenBezeichnung[fahrkartenId]);
		
		zuZahlenderBetrag = fahrkartenPreis[fahrkartenId];
		
		System.out.print("Anzahl der Fahrkarten: ");
		anzahlKarten = tastatur.nextInt();

		while(anzahlKarten < 1 | anzahlKarten > 10) {
			System.out.println("Wert ungueltig. Nur Werte von 1 - 10 erlaubt.");
			anzahlKarten = tastatur.nextInt();
		}

		zuZahlenderBetrag = zuZahlenderBetrag * anzahlKarten;
		zuZahlenderBetrag = Math.round((zuZahlenderBetrag) * 100.0) / 100.0;

		return zuZahlenderBetrag;
		
	}

	public static double fahrkartenBezahlen(double preis) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < preis) {
			System.out
					.println("Noch zu zahlen: "
							+ String.format("%.2f",
									(Math.round((preis - eingezahlterGesamtbetrag) * 100.0)) / 100.0)
							+ " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return  Math.round((eingezahlterGesamtbetrag - preis) * 100.0) / 100.0;

	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}
	
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " Euro");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round((rückgabebetrag) * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round((rückgabebetrag) * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round((rückgabebetrag) * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round((rückgabebetrag) * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round((rückgabebetrag) * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round((rückgabebetrag) * 100.0) / 100.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static void main(String[] args) {
		
		while (true) {
			double a = fahrkartenBezahlen(fahrkartenbestellungErfassen());
			fahrkartenAusgeben();
			rueckgeldAusgeben(a);
			System.out.println("");
		}
		
	}
}